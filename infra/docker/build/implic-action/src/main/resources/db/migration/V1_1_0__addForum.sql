CREATE TABLE categorie_thread(
    "id"             bigserial PRIMARY KEY ,
    "name"           Varchar (50) NOT NULL ,
    "description"    text NOT NULL
);

CREATE TYPE state_thread AS ENUM
(
    'open',
    'closed'
);

CREATE TABLE thread(
    "id"               bigserial PRIMARY KEY ,
    "tittle"           Varchar (200) NOT NULL ,
    "date"             Date NOT NULL ,
    "state"            state_thread NOT NULL DEFAULT 'open',
    "tags"             text,
    "ref_categorie"     bigserial REFERENCES "categorie_thread"(id) NOT NULL,
    "ref_user"         bigserial REFERENCES "user"(id) NOT NULL
);


CREATE TABLE answer(
    "id"                  bigserial PRIMARY KEY ,
    "content"             Varchar (10000) NOT NULL ,
    "date"                DATE NOT NULL,
    "ref_thread"          bigserial REFERENCES "thread"(id) NOT NULL ,
    "ref_user"            bigserial REFERENCES "user"(id) NOT NULL
);
