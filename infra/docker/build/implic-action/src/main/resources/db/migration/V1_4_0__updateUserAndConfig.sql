alter table user_config add column id bigserial not null constraint user_config_pkey primary key;

alter table user_config drop constraint user_config_ref_user_fkey;

alter table user_config drop column ref_user;
alter table user_config drop column visibility_email;
alter table user_config drop column visibility_nom_prenom;
alter table user_config drop column visibility_genre;
alter table user_config drop column visibility_anniversaire;
alter table user_config drop column visibility_telephone;
alter table user_config drop column visibility_adresse;
alter table user_config drop column visibility_code_postal;
alter table user_config drop column visibility_ville;
alter table user_config drop column visibility_departement;
alter table user_config drop column visibility_region;
alter table user_config drop column visibility_pays;
alter table user_config drop column visibility_presentation;
alter table user_config drop column visibility_objectif_pro;
alter table user_config drop column visibility_apport_reseau;
alter table user_config drop column visibility_attente_reseau;
alter table user_config drop column visibility_armee_origine;
alter table user_config drop column visibility_corps_statutaire;
alter table user_config drop column visibility_experience;
alter table user_config drop column visibility_formation;
alter table user_config drop column visibility_interets;
alter table user_config drop column visibility_description_emploi;
alter table user_config drop column visibility_type_emploi;
alter table user_config drop column visibility_citation;

alter table user_config add column visibility_profile text not null;

alter table implic_user add column ref_config bigserial not null;

alter table implic_user add constraint user_config_ref_fkey foreign key (ref_config) references user_config (id);
