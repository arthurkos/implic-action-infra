package io.ekip.implicactioncore.controller;

import io.ekip.implicactioncore.dto.PostResponse;
import io.ekip.implicactioncore.dto.ReplyResponse;
import io.ekip.implicactioncore.dto.form.ReplyForm;
import io.ekip.implicactioncore.service.PostService;
import io.ekip.implicactioncore.service.ReplyService;
import lombok.AllArgsConstructor;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;

@Controller
@AllArgsConstructor
public class ReplyController {
    ReplyService replyService;
    PostService postService;


    @GetMapping("/reply/creer")
    public String showCreerReply(ReplyForm replyForm) {
        return "createReply";
    }

    @PostMapping("/reply/creer")
    public String creerReply(@Valid @ModelAttribute("replyReply") ReplyForm replyForm,
                             BindingResult formValidation,
                             RedirectAttributes attr,
                             @RequestParam("id_post") Long postId,
                             Principal principal) {
        if (formValidation.hasErrors()) {
            attr.addFlashAttribute("org.springframework.validation.BindingResult.replyForm" + postId, formValidation);
            attr.addFlashAttribute("replyForm" + postId, replyForm);
            PostResponse postResponse = postService.getPostById(postId);
            return "redirect:/groupes/" + postResponse.getGroupe().getId();
        }

        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();
        ReplyResponse replyResponse = replyService.saveReply(replyForm, postId, email);

        return "redirect:/groupes/" + replyResponse.getPost().getGroupe().getId();
    }

    @GetMapping("/reply/{id}")
    public String detailsReply(Model model, @PathVariable(value = "id") Long idReply) {
        ReplyResponse replyResponse = replyService.getReplyById(idReply);
        model.addAttribute("replydetails", replyResponse);
        return "infosReply";
    }


}
