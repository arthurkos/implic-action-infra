package io.ekip.implicactioncore.repository;

import io.ekip.implicactioncore.model.PostEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<PostEntity, Long> {

    PostEntity findPostEntityById(Long idPost);
}
