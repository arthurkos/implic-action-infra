package io.ekip.implicactioncore.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum Region implements ImplicBaseEnum<String> {

    //Region code (Code ISO 3166-2)
    FR_ARA("FR_ARA", "Auvergne-Rhône-Alpes"),
    FR_BFC("FR_BFC", "Bourgogne-Franche-Comté"),
    FR_BRE("FR_BRE", "Bretagne"),
    FR_CVL("FR_CVL", "Centre-Val de Loire"),
    FR_COR("FR_COR", "Corse"),
    FR_GES("FR_GES", "Grand Est"),
    FR_HDF("FR_HDF", "Hauts-de-France"),
    FR_IDF("FR_IDF", "Île-de-France"),
    FR_NOR("FR_NOR", "Normandie"),
    FR_NAQ("FR_NAQ", "Nouvelle-Aquitaine"),
    FR_OCC("FR_OCC", "Occitanie"),
    FR_PDL("FR_PDL", "Pays de la Loire"),
    FR_PAC("FR_PAC", "Provence-Alpes-Côte d'Azur"),
    NONE("NONE", "---"),
    ;

    private final String value;
    private final String label;

    public static Region fromEntity(io.ekip.implicactioncore.model.Region regionEntity) {
        if (regionEntity == null) return null;

        return Arrays.stream(values())
                .filter(d -> d.name().equals(regionEntity.name()))
                .findFirst()
                .orElse(null);
    }

    public static Region fromValue(String region) {
        if (region == null) return null;

        return Arrays.stream(values())
                .filter(d -> d.value.equals(region))
                .findFirst()
                .orElse(null);
    }
}
