package io.ekip.implicactioncore.model.converter;

import io.ekip.implicactioncore.model.StatutOffre;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class StatutOffreConverter implements AttributeConverter<StatutOffre, String> {
    @Override
    public String convertToDatabaseColumn(StatutOffre statutOffre) {
        return statutOffre.getLabel();
    }

    @Override
    public StatutOffre convertToEntityAttribute(String string) {
        return StatutOffre.valueOf(string.toUpperCase());
    }
}