package io.ekip.implicactioncore.dto.form.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = MatchValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValuesMatch {
    String message() default "les valeurs correspondent pas.";

    String firstValue();
    String secondValue();

    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
