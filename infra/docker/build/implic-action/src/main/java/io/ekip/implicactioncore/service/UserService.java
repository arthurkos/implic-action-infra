package io.ekip.implicactioncore.service;

import io.ekip.implicactioncore.adapter.UserMapper;
import io.ekip.implicactioncore.dto.GroupResponse;
import io.ekip.implicactioncore.dto.UserResponse;
import io.ekip.implicactioncore.dto.exception.ImplicRuntimeException;
import io.ekip.implicactioncore.dto.form.UserEditProfileForm;
import io.ekip.implicactioncore.dto.form.UserRegistrationForm;
import io.ekip.implicactioncore.model.UserConfigEntity;
import io.ekip.implicactioncore.model.UserEntity;
import io.ekip.implicactioncore.model.Visibility;
import io.ekip.implicactioncore.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class UserService {

    UserRepository userRepository;
    GroupService groupService;

    public void register(UserRegistrationForm userForm, LocalDate registerDate, String name, String email) {
        var maybeUser = userRepository.getByEmail(email);

        if (maybeUser.isPresent()) {
            throw new ImplicRuntimeException("L'email est deja utilisé");
        } else {
            userRepository.save(UserMapper.entityFromForm(userForm, registerDate, name, email, true, true, false));
        }
    }

    public void delete(Long userId) {
        UserEntity userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new ImplicRuntimeException("Utilisateur non trouvé."));
        userRepository.delete(userEntity);
    }

    public UserResponse getUserProfile(String email) {
        UserEntity userEntity = userRepository.getByEmail(email)
                .orElseThrow(() -> new ImplicRuntimeException("Utilisateur non trouvé."));

        List<GroupResponse> groupEntityList = groupService.getAllGroupbyUser(userEntity.getId());
        return UserMapper.ResponseFromEntity(userEntity, groupEntityList);
    }

    public UserResponse getUserProfile(Long userId) {
        UserEntity userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new ImplicRuntimeException("Utilisateur non trouvé."));

        List<GroupResponse> groupEntityList = groupService.getAllGroupbyUser(userEntity.getId());
        return UserMapper.ResponseFromEntity(userEntity, groupEntityList);
    }

    public UserResponse editUserProfile(UserEditProfileForm userForm, LocalDate editDate, String name, String email) {
        var maybeUser = userRepository.getByEmail(email);
        var updatedUser = UserMapper.profileEntityFromForm(userForm, editDate, name, email);
        if (maybeUser.isPresent()) {
            updatedUser.setId(maybeUser.get().getId());
            UserConfigEntity userConfigEntity = maybeUser.get().getUserConfigEntity();
            userConfigEntity.setVisibilityProfile(Visibility.fromValue(userForm.getVisibiliteProfile()));
            updatedUser.setUserConfigEntity(userConfigEntity);
            return UserMapper.ResponseFromEntity(userRepository.save(updatedUser));
        } else {
            throw new ImplicRuntimeException("L'utilisateur n'existe pas !");
        }
    }

    public boolean userIsRegistered(String email) {
        Optional<UserEntity> entity = userRepository.getByEmail(email);
        return entity.isPresent();
    }

    public boolean userIsApproved(String email) {
        Optional<UserEntity> mayBeEntity = userRepository.getByEmail(email);
        return mayBeEntity.map(user -> user.getUserConfigEntity().isApproved())
                .orElse(false);
    }

    public List<UserResponse> getAllUser() {
        return userRepository.findAll().stream()
                .map(UserMapper::ResponseFromEntity)
                .collect(Collectors.toList());
    }

    public void approveDisapproveUserById(Long userId) {
        UserEntity userEntity = userRepository.findById(userId)
                .orElseThrow(() -> new ImplicRuntimeException("Utilisateur non trouvé."));
        UserConfigEntity userConfig = userEntity.getUserConfigEntity();
        userConfig.setApproved(!userConfig.isApproved());
        userEntity.setUserConfigEntity(userConfig);
        userRepository.save(userEntity);
    }

}
