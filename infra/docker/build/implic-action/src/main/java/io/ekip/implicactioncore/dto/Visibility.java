package io.ekip.implicactioncore.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum Visibility implements ImplicBaseEnum<String>{

    PRIVATE("private", "Privé"),
    FRIENDS("friends", "Ami.e.s"),
    ALL("all", "Tout le monde"),
    MEMBER("members", "Membres")
    ;

    private final String value;
    private final String label;

    public static Visibility fromValue(String value) {
        return Arrays.stream(values())
                .filter(visibility -> visibility.getValue().equals(value))
                .findFirst()
                .orElse(Visibility.PRIVATE);
    }

}
