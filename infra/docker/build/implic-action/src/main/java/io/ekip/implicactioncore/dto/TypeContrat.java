package io.ekip.implicactioncore.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum TypeContrat implements ImplicBaseEnum<String> {

    CDI("cdi", "CDI"),
    CDD("cdd", "CDD"),
    FREELANCE("freelance", "Freelance"),
    STAGE("stage", "Stage"),
    TEMPSPARTIEL("tempspartiel", "Temps partiel"),
    ;

    private final String value;
    private final String label;

    public static TypeContrat fromValue(String typeContrat) {
        if (typeContrat == null) return null;

        return Arrays.stream(values())
                .filter(d -> d.value.equals(typeContrat))
                .findFirst()
                .orElse(null);
    }
}
