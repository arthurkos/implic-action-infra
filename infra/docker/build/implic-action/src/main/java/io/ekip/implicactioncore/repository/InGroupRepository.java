package io.ekip.implicactioncore.repository;

import io.ekip.implicactioncore.model.InGroupEntity;
import io.ekip.implicactioncore.model.InGroupKey;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InGroupRepository extends JpaRepository<InGroupEntity, InGroupKey> {
    List<InGroupEntity> getInGroupEntityByUserId(Long id);
    List<InGroupEntity> getInGroupEntityByGroupId(Long id);
}
