package io.ekip.implicactioncore.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "user_config")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserConfigEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Column(name = "visibility_profile")
    private Visibility visibilityProfile;

    @Column(name = "last_cgu_accepted")
    private boolean lastCguAccepted;

    @Column(name = "is_active")
    private boolean isActive;

    @Column(name = "is_approved")
    private boolean isApproved;
}
