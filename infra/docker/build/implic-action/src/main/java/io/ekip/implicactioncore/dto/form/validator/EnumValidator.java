package io.ekip.implicactioncore.dto.form.validator;

import io.ekip.implicactioncore.dto.ImplicBaseEnum;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EnumValidator implements ConstraintValidator<EnumConstraint, String> {

    private List<String> acceptedValues;

    @Override
    public void initialize(EnumConstraint constraintAnnotation) {
        acceptedValues = Stream.of(constraintAnnotation.enumClass().getEnumConstants())
                .map(ImplicBaseEnum::getValue)
                .collect(Collectors.toList());
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.equals("")) { //Allow null value, should add @NotNull to verify this condition
            return true;
        }
        return acceptedValues.contains(value);
    }


}
