package io.ekip.implicactioncore.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "groupe")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class GroupEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "desc_group")
    private String desc;

    @Column(name = "region")
    private Region region;

    @OneToMany(mappedBy = "groupe", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PostEntity> post;

}
