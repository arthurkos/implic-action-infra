package io.ekip.implicactioncore.service;

import io.ekip.implicactioncore.adapter.GroupMapper;
import io.ekip.implicactioncore.adapter.UserMapper;
import io.ekip.implicactioncore.dto.GroupResponse;
import io.ekip.implicactioncore.dto.UserResponse;
import io.ekip.implicactioncore.dto.exception.ImplicRuntimeException;
import io.ekip.implicactioncore.dto.form.GroupForm;
import io.ekip.implicactioncore.model.GroupEntity;
import io.ekip.implicactioncore.model.InGroupEntity;
import io.ekip.implicactioncore.repository.GroupRepository;
import io.ekip.implicactioncore.repository.InGroupRepository;
import io.ekip.implicactioncore.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class GroupService {
    GroupRepository groupRepository;
    UserRepository userRepository;
    InGroupRepository inGroupRepository;

    public GroupResponse saveGroup(GroupForm groupForm) {
        GroupEntity groupEntity = GroupMapper.entityFromForm(groupForm);
        groupRepository.save(groupEntity);
        return GroupMapper.ResponseFromEntity(groupEntity);
    }

    public void delete(Long idGroup) {
        GroupEntity groupEntity = groupRepository.findById(idGroup)
                .orElseThrow(() -> new ImplicRuntimeException("Aucun groupe n'existe pour cet id: " + idGroup));
        List<InGroupEntity> inGroupEntities = inGroupRepository.getInGroupEntityByGroupId(idGroup);
        inGroupRepository.deleteAll(inGroupEntities);
        groupRepository.delete(groupEntity);
    }

    public List<GroupResponse> getAllGroup() {
        return groupRepository.findAll().stream()
                .map(GroupMapper::ResponseFromEntity)
                .collect(Collectors.toList());
    }

    public GroupResponse getGroupById(Long id) {
        Optional<GroupEntity> maybeGroup = groupRepository.findById(id);
        return maybeGroup
                .map(GroupMapper::ResponseFromEntity)
                .orElseThrow(() -> new ImplicRuntimeException("Aucun groupe n'existe pour cet id: " + id));
    }

    public GroupForm getGroupFormbyId(Long idGroup) {
        Optional<GroupEntity> maybeGroup = groupRepository.findById(idGroup);
        return maybeGroup
                .map(GroupMapper::formFromEntity)
                .orElseThrow(() -> new ImplicRuntimeException("Aucun groupe n'existe pour cet id: " + idGroup));
    }

    public List<GroupResponse> getAllGroupbyUser(Long userId) {
        return inGroupRepository.getInGroupEntityByUserId(userId).stream()
                .map(inGroupEntity -> groupRepository.findById(inGroupEntity.getGroupId()).orElse(null))
                .filter(Objects::nonNull)
                .map(GroupMapper::ResponseFromEntity)
                .collect(Collectors.toList());
    }

    public GroupResponse updateGroup(GroupForm groupForm, Long idGroup) {
        GroupEntity groupToUpdate = groupRepository.findById(idGroup)
                .orElseThrow(() -> new ImplicRuntimeException("Utilisateur non trouvé."));

        GroupEntity groupUpdated = GroupMapper.entityFromForm(groupForm);

        groupToUpdate.setId(idGroup);
        groupToUpdate.setName(groupUpdated.getName());
        groupToUpdate.setDesc(groupUpdated.getDesc());
        groupToUpdate.setRegion(groupUpdated.getRegion());

        groupRepository.save(groupToUpdate);
        return GroupMapper.ResponseFromEntity(groupToUpdate);
    }

    public List<UserResponse> getUsersFromGroupId(Long Groupid) {
        return inGroupRepository.getInGroupEntityByGroupId(Groupid).stream()
                .map(inGroupEntity -> userRepository.findById(inGroupEntity.getUserId()).orElse(null))
                .filter(Objects::nonNull)
                .map(user -> UserMapper.ResponseFromEntity(user, getAllGroupbyUser(user.getId())))
                .collect(Collectors.toList());
    }

}
