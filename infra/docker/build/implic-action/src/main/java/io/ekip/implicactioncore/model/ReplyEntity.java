package io.ekip.implicactioncore.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "post_reply")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReplyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "message")
    private String message;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ref_post", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private PostEntity post;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ref_user", nullable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private UserEntity user;

}
