package io.ekip.implicactioncore.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "categorie_emploi")
public class CategorieEmploiEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "nom_categorie")
    private String nomCategorie;

//    @ManyToMany(mappedBy = "lesCategoriesEmploi")
//    Set<OffreEntity> lesOffres;

    public CategorieEmploiEntity() {
    }

    public CategorieEmploiEntity(long id, String nomCategorie) {
        this.id = id;
        this.nomCategorie = nomCategorie;
    }

    public long getId() {
        return id;
    }

    public String getNomCategorie() {
        return nomCategorie;
    }

    public void setNomCategorie(String nomCategorie) {
        this.nomCategorie = nomCategorie;
    }
}

