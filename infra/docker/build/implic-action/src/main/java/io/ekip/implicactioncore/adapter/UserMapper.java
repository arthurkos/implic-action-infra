package io.ekip.implicactioncore.adapter;

import io.ekip.implicactioncore.dto.GroupResponse;
import io.ekip.implicactioncore.dto.Region;
import io.ekip.implicactioncore.dto.UserResponse;
import io.ekip.implicactioncore.dto.form.UserEditProfileForm;
import io.ekip.implicactioncore.dto.form.UserRegistrationForm;
import io.ekip.implicactioncore.model.Genre;
import io.ekip.implicactioncore.model.UserEntity;
import io.ekip.implicactioncore.utils.ImplicArraysUtils;
import lombok.experimental.UtilityClass;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@UtilityClass
public class UserMapper {


    public static UserEntity entityFromForm(UserRegistrationForm form,
                                            LocalDate dateInscription,
                                            String nom,
                                            String email,
                                            boolean acceptedCGU,
                                            boolean isActive,
                                            boolean isApproved) {

        return new UserEntity(
                null,
                UserConfigMapper.entityFromUserForm(form, acceptedCGU, isActive, isApproved),
                email,
                nom,
                Genre.valueOf(form.getGenre().toUpperCase()),
                form.getBirthdate(),
                form.getTelephone(),
                form.getAdresse(),
                null,
                null,
                null,
                io.ekip.implicactioncore.model.Region.valueOf(Region.fromValue(form.getRegion()).getValue()),
                null,
                form.getPresentation(),
                form.getObjectifPro(),
                form.getApportReseau(),
                form.getAttenteReseau(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                Collections.emptyList(),
                null,
                Collections.emptyList(),
                null,
                dateInscription,
                Collections.emptySet(),
                Collections.emptySet()
        );
    }

    public static UserEntity profileEntityFromForm(UserEditProfileForm form,
                                                   LocalDate dateModification,
                                                   String nom,
                                                   String email) {
        return new UserEntity(
                null,
                null,
                email,
                nom,
                Genre.valueOf(form.getGenre().toUpperCase()),
                form.getBirthdate(),
                form.getTelephone(),
                form.getAdresse(),
                form.getVille(),
                form.getCodePostal(),
                form.getDepartement(),
                form.getRegion(),
                form.getPays(),
                form.getPresentation(),
                form.getObjectifPro(),
                form.getApportReseau(),
                form.getAttenteReseau(),
                form.getArmeeOrigine(),
                form.getCorpsStatutaire(),
                ImplicArraysUtils.allElementsAreNull(form.getDateExperience()) ? Collections.emptyList() :
                        Arrays.stream(form.getDateExperience())
                                .filter(Objects::nonNull) // we authorize any number of elements (not a period)
                                .map(Date::valueOf)
                                .collect(Collectors.toList()),
                form.getDescriptionExperience(),
                ImplicArraysUtils.allElementsAreNull(form.getDateFormation()) ? Collections.emptyList() :
                        Arrays.stream(form.getDateFormation())
                                .filter(Objects::nonNull) // we authorize any number of elements (not a period)
                                .map(Date::valueOf)
                                .collect(Collectors.toList()),
                form.getDescriptionFormation(),
                form.getInterets(),
                form.getDescriptionEmploi(),
                form.getTypeEmploi(),
                form.getCitation(),
                dateModification,
                Collections.emptySet(),
                Collections.emptySet()
        );
    }

    public static UserResponse ResponseFromEntity(UserEntity entity, List<GroupResponse> groups) {
        return new UserResponse(
                entity.getId(),
                entity.getEmail(),
                entity.getNomPrenom(),
                entity.getBirthdate(),
                io.ekip.implicactioncore.dto.Genre.valueOf(entity.getGenre().name().toUpperCase()),
                entity.getTelephone(),
                entity.getAdresse(),
                entity.getVille(),
                entity.getCodePostal(),
                entity.getDepartement(),
                Region.fromEntity(entity.getRegion()),
                io.ekip.implicactioncore.dto.Visibility.valueOf(entity.getUserConfigEntity().getVisibilityProfile().name().toUpperCase()),
                entity.getPays(),
                entity.getPresentation(),
                entity.getObjectifPro(),
                entity.getApportReseau(),
                entity.getAttenteReseau(),
                entity.getArmeeOrigine(),
                entity.getCorpsStatutaire(),
                entity.getDateExperience(),
                entity.getDescriptionExperience(),
                entity.getDateFormation(),
                entity.getDescriptionFormation(),
                entity.getInterets(),
                entity.getDescriptionEmploi(),
                entity.getTypeEmploi(),
                entity.getCitation(),
                groups,
                entity.getUserConfigEntity().isApproved()
        );
    }

    public static UserResponse ResponseFromEntity(UserEntity entity) {
        return new UserResponse(
                entity.getId(),
                entity.getEmail(),
                entity.getNomPrenom(),
                entity.getBirthdate(),
                io.ekip.implicactioncore.dto.Genre.valueOf(entity.getGenre().name().toUpperCase()),
                entity.getTelephone(),
                entity.getAdresse(),
                entity.getVille(),
                entity.getCodePostal(),
                entity.getDepartement(),
                Region.fromEntity(entity.getRegion()),
                io.ekip.implicactioncore.dto.Visibility.valueOf(entity.getUserConfigEntity().getVisibilityProfile().name().toUpperCase()),
                entity.getPays(),
                entity.getPresentation(),
                entity.getObjectifPro(),
                entity.getApportReseau(),
                entity.getAttenteReseau(),
                entity.getArmeeOrigine(),
                entity.getCorpsStatutaire(),
                entity.getDateExperience(),
                entity.getDescriptionExperience(),
                entity.getDateFormation(),
                entity.getDescriptionFormation(),
                entity.getInterets(),
                entity.getDescriptionEmploi(),
                entity.getTypeEmploi(),
                entity.getCitation(),
                null,
                entity.getUserConfigEntity().isApproved()
        );
    }

    public static UserEditProfileForm editFormFromUserResponse(UserResponse response) {
        return new UserEditProfileForm(
                io.ekip.implicactioncore.dto.Genre.valueOf(response.getGenre().name().toUpperCase()).getValue(),
                response.getBirthdate(),
                response.getTelephone(),
                response.getAdresse(),
                response.getVille(),
                response.getCodePostal(),
                response.getDepartement(),
                io.ekip.implicactioncore.model.Region.valueOf(response.getRegion().getValue()),
                response.getVisibility().getValue(),
                response.getPays(),
                response.getArmeeOrigine(),
                response.getCorpsStatutaire(),
                response.getDateExperience().toArray(LocalDate[]::new),
                response.getDescriptionExperience(),
                response.getDateFormation().toArray(LocalDate[]::new),
                response.getDescriptionFormation(),
                response.getInterets(),
                response.getDescriptionEmploi(),
                response.getTypeEmploi(),
                response.getCitation(),
                response.getPresentation(),
                response.getObjectifPro(),
                response.getApportReseau(),
                response.getAttenteReseau()
        );
    }
}
