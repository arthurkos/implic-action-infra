package io.ekip.implicactioncore.dto;

import io.ekip.implicactioncore.model.PostEntity;
import io.ekip.implicactioncore.model.UserEntity;
import lombok.Value;


@Value
public class ReplyResponse {
    Long id;
    String message;
    PostEntity post;
    UserEntity user;
}
