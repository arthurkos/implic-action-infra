package io.ekip.implicactioncore.adapter;

import io.ekip.implicactioncore.dto.form.UserRegistrationForm;
import io.ekip.implicactioncore.model.UserConfigEntity;
import io.ekip.implicactioncore.model.Visibility;
import lombok.experimental.UtilityClass;

@UtilityClass
public class UserConfigMapper {

    public static UserConfigEntity entityFromUserForm(UserRegistrationForm form, boolean acceptedCGU, boolean isActive, boolean isApproved) {
        return new UserConfigEntity(
                null,
                Visibility.fromDto(io.ekip.implicactioncore.dto.Visibility.fromValue(form.getVisibiliteProfile())),
                acceptedCGU,
                isActive,
                isApproved
        );
    }

}
