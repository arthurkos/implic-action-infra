package io.ekip.implicactioncore.model.converter;

import io.ekip.implicactioncore.model.Genre;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class GenreAttributeConverter implements AttributeConverter<Genre, String> {
    @Override
    public String convertToDatabaseColumn(Genre genre) {
        if(genre == null) {
            return null;
        }
        return genre.getLabel();
    }

    @Override
    public Genre convertToEntityAttribute(String rawGenre) {
        if(rawGenre == null) {
            return null;
        }
        return Genre.valueOf(rawGenre.toUpperCase());
    }
}
