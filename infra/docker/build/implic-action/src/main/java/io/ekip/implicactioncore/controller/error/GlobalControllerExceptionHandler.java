package io.ekip.implicactioncore.controller.error;

import io.ekip.implicactioncore.dto.exception.ImplicRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(ImplicRuntimeException.class)
    public ModelAndView genericExceptionHandler(ImplicRuntimeException exception) {

        return this.configureErrorPage(exception, HttpStatus.INTERNAL_SERVER_ERROR, "/error/errorPage");
    }


    private ModelAndView configureErrorPage(Exception exception, HttpStatus status, String template) {

        String errorMessage = exception.getMessage() == null || exception.getMessage().equals("") ?
                "[pas de message précisé]" : exception.getMessage();

        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", exception);
        mav.addObject("statusCode", status);
        mav.addObject("errorMessage", errorMessage);

        mav.setViewName(template);

        return mav;
    }

}
