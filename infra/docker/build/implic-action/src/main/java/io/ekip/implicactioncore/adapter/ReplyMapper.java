package io.ekip.implicactioncore.adapter;

import io.ekip.implicactioncore.dto.ReplyResponse;
import io.ekip.implicactioncore.dto.form.ReplyForm;
import io.ekip.implicactioncore.model.PostEntity;
import io.ekip.implicactioncore.model.ReplyEntity;
import io.ekip.implicactioncore.model.UserEntity;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ReplyMapper {
    public static ReplyEntity entityFromForm(ReplyForm replyForm, PostEntity postEntity, UserEntity userEntity) {
        return new ReplyEntity(
                null,
                replyForm.getMessage(),
                postEntity,
                userEntity
        );
    }


    public static ReplyResponse responseFromEntity(ReplyEntity replyEntity) {
        return new ReplyResponse(
                replyEntity.getId(),
                replyEntity.getMessage(),
                replyEntity.getPost(),
                replyEntity.getUser()
        );
    }

    public static ReplyForm formFromEntity(ReplyEntity replyEntity) {
        return new ReplyForm(
                replyEntity.getMessage()
        );
    }


}
