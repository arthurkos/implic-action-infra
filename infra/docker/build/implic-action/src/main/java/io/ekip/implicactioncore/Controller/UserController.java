package io.ekip.implicactioncore.controller;

import io.ekip.implicactioncore.adapter.UserMapper;
import io.ekip.implicactioncore.dto.UserResponse;
import io.ekip.implicactioncore.dto.form.UserEditProfileForm;
import io.ekip.implicactioncore.dto.form.UserRegistrationForm;
import io.ekip.implicactioncore.service.UserService;
import lombok.AllArgsConstructor;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;

@Controller
@AllArgsConstructor
public class UserController {

    UserService userService;

    @GetMapping("/register")
    @RolesAllowed(value = {"ROLE_USER", "ROLE_ADMIN"})
    public String showRegistrationForm(UserRegistrationForm userRegistrationForm, Principal principal, Model model) {
        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();
        String name = accessToken.getName();

        model.addAttribute("email", email);
        model.addAttribute("name", name);
        return "register";
    }

    @PostMapping("/register")
    @RolesAllowed(value = {"ROLE_USER", "ROLE_ADMIN"})
    public String registerUserAccount(@Valid @ModelAttribute("userRegistrationForm") UserRegistrationForm userRegistrationForm,
                                      BindingResult formValidation,
                                      Principal principal,
                                      Model model) {
        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String name = accessToken.getName();
        String email = accessToken.getEmail();
        if (formValidation.hasErrors()) {
            model.addAttribute("email", email);
            model.addAttribute("name", name);
            return "register";
        }
        userService.register(userRegistrationForm, LocalDate.now(), name, email);
        return "waitingApproval";
    }

    @GetMapping("/profil")
    @RolesAllowed(value = {"ROLE_USER", "ROLE_ADMIN"})
    public String showUserProfile(UserEditProfileForm userEditProfileForm, Principal principal, Model model) {
        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String name = accessToken.getName();
        String email = accessToken.getEmail();

        UserResponse user = userService.getUserProfile(email);
        model.addAttribute("email", email);
        model.addAttribute("name", name);
        model.addAttribute("user", user);
        model.addAttribute("listGroups", user.getGroups());
        model.addAttribute("userEditProfileForm", UserMapper.editFormFromUserResponse(user));
        
        return "profile";
    }

    @GetMapping("/profil/editer")
    @RolesAllowed(value = {"ROLE_USER", "ROLE_ADMIN"})
    public String showEditProfileForm(UserEditProfileForm userEditProfileForm, Principal principal, Model model) {
        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();

        UserResponse user = userService.getUserProfile(email);

        model.addAttribute("user", user);
        model.addAttribute("userEditProfileForm", UserMapper.editFormFromUserResponse(user));
        return "editProfile";
    }

    @PostMapping("/profil/editer")
    @RolesAllowed(value = {"ROLE_USER", "ROLE_ADMIN"})
    public String editUserProfile(@Valid @ModelAttribute("userEditProfileForm") UserEditProfileForm userEditProfileForm,
                                  BindingResult formValidation,
                                  Principal principal,
                                  Model model) {

        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String name = accessToken.getName();
        String email = accessToken.getEmail();
        model.addAttribute("email", email);
        model.addAttribute("name", name);

        if (formValidation.hasErrors()) {
            return "profile";
        }

        UserResponse user = userService.editUserProfile(userEditProfileForm, LocalDate.now(), name, email);
        model.addAttribute("user", user);
        return "profile";
    }

    @GetMapping(path = "/logout")
    public String logout(HttpServletRequest request) throws ServletException {
        request.logout();
        return "redirect:/accueil";
    }

    @GetMapping("/waitingApproval")
    @RolesAllowed(value = {"ROLE_USER", "ROLE_ADMIN"})
    public String showWaitingApproval(Principal principal) {
        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();

        if(userService.userIsApproved(email)) {
            return "redirect:/";
        }

        return "waitingApproval";
    }

    @GetMapping("/profil/{id}/approuver")
    @RolesAllowed("ROLE_ADMIN")
    public String approveUser(@PathVariable(value = "id") Long idUser) {
        userService.approveDisapproveUserById(idUser);
        return "redirect:/administration";
    }

    @GetMapping("/profil/{id}/editer")
    @RolesAllowed("ROLE_ADMIN")
    public String adminEditProfileForm(@PathVariable(value = "id") Long idUser, UserEditProfileForm userEditProfileForm, Principal principal, Model model) {
        UserResponse user = userService.getUserProfile(idUser);
        model.addAttribute("email", user.getEmail());
        model.addAttribute("name", user.getNomPrenom());
        model.addAttribute("user", user);
        model.addAttribute("userEditProfileForm", UserMapper.editFormFromUserResponse(user));
        return "editProfileAdmin";
    }

    @PostMapping("/profil/{id}/editer")
    @RolesAllowed("ROLE_ADMIN")
    public String adminEditUserProfile(@PathVariable(value = "id") Long idUser, @Valid @ModelAttribute("userEditProfileForm") UserEditProfileForm userEditProfileForm,
                                  BindingResult formValidation,
                                  Principal principal,
                                  Model model) {
        UserResponse user = userService.getUserProfile(idUser);

        model.addAttribute("email",user.getEmail());
        model.addAttribute("name", user.getNomPrenom());

        if (formValidation.hasErrors()) {
            return "editProfileAdmin";
        }

        UserResponse updatedUser = userService.editUserProfile(userEditProfileForm, LocalDate.now(), user.getNomPrenom(), user.getEmail());
        model.addAttribute("user", updatedUser);

        return "editProfileAdmin";
    }

    @GetMapping("/profil/{id}/supprimer")
    @RolesAllowed("ROLE_ADMIN")
    public String adminDeleteUserProfile(@PathVariable(value = "id") Long idUser, @Valid @ModelAttribute("userEditProfileForm") UserEditProfileForm userEditProfileForm,
                                       BindingResult formValidation,
                                       Principal principal,
                                       Model model) {
        userService.delete(idUser);
        return "redirect:/administration";
    }

}
