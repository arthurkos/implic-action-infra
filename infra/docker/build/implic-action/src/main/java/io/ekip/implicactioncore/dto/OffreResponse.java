package io.ekip.implicactioncore.dto;

import lombok.Value;

import java.time.LocalDate;

@Value
public class OffreResponse {

    Long id;
    LocalDate datePublication;
    String titrePoste;
    TypeContrat typePoste;
    String description;
    String email;
    String nomEntreprise;
    String urlEntreprise;
    String descEntreprise;
    String urlVideo;
    String twitter;
    String lieuPoste;
    StatutOffre statut;

}
