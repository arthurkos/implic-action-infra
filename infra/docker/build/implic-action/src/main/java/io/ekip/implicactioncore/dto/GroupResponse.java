package io.ekip.implicactioncore.dto;

import lombok.Value;

import java.util.List;

@Value
public class GroupResponse {
    Long id;
    String name;
    String desc;
    Region region;
    List<PostResponse> postList;
}
