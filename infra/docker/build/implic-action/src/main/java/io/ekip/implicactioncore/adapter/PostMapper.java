package io.ekip.implicactioncore.adapter;

import io.ekip.implicactioncore.dto.PostResponse;
import io.ekip.implicactioncore.dto.ReplyResponse;
import io.ekip.implicactioncore.dto.form.PostForm;
import io.ekip.implicactioncore.model.GroupEntity;
import io.ekip.implicactioncore.model.PostEntity;
import io.ekip.implicactioncore.model.UserEntity;
import lombok.experimental.UtilityClass;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

@UtilityClass
public class PostMapper {
    public static PostEntity entityFromForm(PostForm postForm, UserEntity userEntity, LocalDate creationDate, GroupEntity groupEntity) {
        return new PostEntity(
                null,
                postForm.getName(),
                postForm.getMessage(),
                creationDate,
                creationDate,
                groupEntity,
                userEntity,
                Collections.emptyList()
        );
    }

    public static PostResponse responseFromEntity(PostEntity postEntity) {
        return new PostResponse(
                postEntity.getId(),
                postEntity.getName(),
                postEntity.getMessage(),
                postEntity.getCreationDate(),
                postEntity.getUpdateDate(),
                postEntity.getGroupe(),
                postEntity.getUser(),
                postEntity.getReply().stream()
                        .map(ReplyMapper::responseFromEntity)
                        .sorted(Comparator.comparing(ReplyResponse::getId))
                        .collect(Collectors.toList())
        );
    }

    public static PostForm formFromEntity(PostEntity postEntity) {
        return new PostForm(
                postEntity.getName(),
                postEntity.getMessage()
        );
    }

}
