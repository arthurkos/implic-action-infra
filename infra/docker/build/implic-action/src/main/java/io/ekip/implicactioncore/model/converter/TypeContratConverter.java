package io.ekip.implicactioncore.model.converter;

import io.ekip.implicactioncore.model.TypeContrat;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TypeContratConverter implements AttributeConverter<TypeContrat, String> {
    @Override
    public String convertToDatabaseColumn(TypeContrat typeContrat) {
        return typeContrat.getLabel();
    }

    @Override
    public TypeContrat convertToEntityAttribute(String string) {
        return TypeContrat.fromLabel(string);
    }
}

