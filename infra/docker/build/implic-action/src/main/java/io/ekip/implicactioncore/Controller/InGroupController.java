package io.ekip.implicactioncore.controller;

import io.ekip.implicactioncore.dto.GroupResponse;
import io.ekip.implicactioncore.dto.UserResponse;
import io.ekip.implicactioncore.service.GroupService;
import io.ekip.implicactioncore.service.InGroupService;
import io.ekip.implicactioncore.service.UserService;
import lombok.AllArgsConstructor;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;
import java.util.List;

@Controller
@AllArgsConstructor
public class InGroupController {
    InGroupService inGroupService;
    UserService userService;
    GroupService groupService;


    @PostMapping("/groupes/abonner")
    public String subscribeGroup(@RequestParam("id_group") Long idGroup, Principal principal, Model model) {

        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();

        UserResponse user = userService.getUserProfile(email);
        GroupResponse groupResponse = inGroupService.subscribeGroup(idGroup, user);
        model.addAttribute("groupdetails", groupResponse);
        return "redirect:/groupes/" + groupResponse.getId();
    }

    @PostMapping("/groupes/desabonner")
    public String unsubscribeGroup(@RequestParam("id_group") Long idGroup, Principal principal, Model model) {

        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();

        UserResponse user = userService.getUserProfile(email);
        GroupResponse groupResponse = inGroupService.unsubscribeGroup(idGroup, user);
        model.addAttribute("groupdetails", groupResponse);
        return "redirect:/groupes/" + groupResponse.getId();
    }

    @RolesAllowed("ROLE_ADMIN")
    @GetMapping("/groupes/moderation")
    public String showModerateGroup(Model model) {
        List<GroupResponse> listGroup = groupService.getAllGroup();
        List<UserResponse> listUser = userService.getAllUser();
        model.addAttribute("listGroups", listGroup);
        model.addAttribute("listUsers", listUser);
        return "moderation";
    }

    @PostMapping("/groupes/moderation")
    public String moderateGroup(Model model, @RequestParam("id_group") Long idGroup,
                                @RequestParam("id_user") Long idUser) {

        inGroupService.setModo(idGroup, idUser);
        return "redirect:/groupes/moderation";
    }
}
