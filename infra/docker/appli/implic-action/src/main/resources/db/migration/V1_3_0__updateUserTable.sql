alter table "user" rename to implic_user;

alter table implic_user alter column genre TYPE text;

alter table implic_user drop constraint user_username_key;

alter table implic_user drop username;

create unique index implic_user_email_uindex on implic_user (email);

alter table user_config drop constraint user_config_ref_user_fkey;

alter table user_config add constraint user_config_ref_user_fkey foreign key (ref_user) references implic_user (id);

alter table offre drop constraint offre_ref_user_fkey;

alter table offre add constraint offre_ref_user_fkey foreign key (ref_user) references implic_user (id);

alter table user_config add column is_approved boolean default false
