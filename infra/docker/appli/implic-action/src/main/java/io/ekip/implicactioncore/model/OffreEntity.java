package io.ekip.implicactioncore.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "OffreEntity")
@Table(name = "offre")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class OffreEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "ref_user")
    private Long user;

    @Column(name = "date_publication")
    private LocalDate datePublication;

    @Column(name = "titre_poste")
    private String titrePoste;

    @Column(name = "type_poste")
    private TypeContrat typeContrat;

//    @ManyToMany TODO : à implémenter
//    @JoinTable(
//            name = "categorie_offre",
//            joinColumns = @JoinColumn(name = "ref_offre"),
//            inverseJoinColumns = @JoinColumn(name = "ref_categorie"))
//    Set<CategorieEmploiEntity> lesCategoriesEmploi;

    @Column(name = "description")
    private String description;

    @Column(name = "email")
    private String email;

    @Column(name = "nom_entreprise")
    private String nomEntreprise;

    @Column(name = "url_entreprise")
    private String urlEntreprise;

    @Column(name = "description_entreprise")
    private String descEntreprise;

    @Column(name = "url_video")
    private String urlVideo;

    @Column(name = "twitter")
    private String twitter;

    @Column(name = "statut")
    private StatutOffre statut;

    @Column(name = "lieu_poste")
    private String lieuPoste;

}
