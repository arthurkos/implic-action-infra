package io.ekip.implicactioncore.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum TypeContrat {

    CDI("cdi"),
    CDD("cdd"),
    FREELANCE("freelance"),
    STAGE("stage"),
    TEMPSPARTIEL("temps_partiel"),
    ;

    private final String label;

    public static TypeContrat fromLabel(String label) {
        if (label == null) return null;

        return Arrays.stream(values())
                .filter(d -> d.label.equals(label))
                .findFirst()
                .orElse(null);
    }
}
