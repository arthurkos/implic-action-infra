package io.ekip.implicactioncore.repository;

import io.ekip.implicactioncore.model.GroupEntity;
import io.ekip.implicactioncore.model.InGroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GroupRepository extends JpaRepository<GroupEntity, Long> {
}
