package io.ekip.implicactioncore.controller;

import io.ekip.implicactioncore.dto.OffreResponse;
import io.ekip.implicactioncore.dto.form.OffreForm;
import io.ekip.implicactioncore.model.StatutOffre;
import io.ekip.implicactioncore.service.OffreService;
import io.ekip.implicactioncore.service.UserService;
import lombok.AllArgsConstructor;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.security.Principal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
public class OffreController {

    private final OffreService offreService;
    private final UserService userService;

    @GetMapping("/offres")
    public String listOffer(Model model,
                            @RequestParam("statut") Optional<List<String>> maybeStatut,
                            @RequestParam("contrat") Optional<List<String>> maybeContrat,
                            @RequestParam("lieu") Optional<String> maybeLieu,
                            @RequestParam("sort") Optional<String> maybeSortByDate) {
        List<OffreResponse> offres = offreService.getAllOffre().stream()
                .filter(offre -> maybeStatut.map(statut -> statut.contains(offre.getStatut().getValue())).orElse(true))
                .filter(offre -> maybeContrat.map(contrat -> contrat.contains(offre.getTypePoste().getValue())).orElse(true))
                .filter(offre -> maybeLieu.map(lieu -> offre.getLieuPoste().toLowerCase().contains(lieu.toLowerCase())).orElse(true))
                .sorted(maybeSortByDate.map(__ -> Comparator.comparing(OffreResponse::getDatePublication))
                        .orElse(Comparator.comparing(OffreResponse::getDatePublication).reversed())
                )
                .collect(Collectors.toList());

        if (maybeContrat.isPresent()) {
            List<String> listContrat = maybeContrat.get();
            listContrat.forEach(contrat -> {
                if (contrat.equals("cdi")) {
                    model.addAttribute("cdiSelected", true);
                }
                if (contrat.equals("cdd")) {
                    model.addAttribute("cddSelected", true);
                }
                if (contrat.equals("freelance")) {
                    model.addAttribute("freelanceSelected", true);
                }
                if (contrat.equals("stage")) {
                    model.addAttribute("stageSelected", true);
                }
                if (contrat.equals("temps_partiel")) {
                    model.addAttribute("temps_partielSelected", true);
                }
            });
        }

        model.addAttribute("listOffres", offres);
        return "listOffers";
    }

    @GetMapping("/offres/{id}")
    public String detailsOffre(Model model, @PathVariable(value = "id") Long idOffre) {
        OffreResponse offre = offreService.getOffreById(idOffre);
        model.addAttribute("offredetails", offre);
        return "infosOffre";
    }

    @GetMapping("/offres/creer")
    @RolesAllowed("ROLE_ADMIN")
    public String showCreateOffre(OffreForm offreForm) {
        return "createOffre";
    }

    @PostMapping("/offres/creer")
    @RolesAllowed("ROLE_ADMIN")
    public String createOffre(@Valid @ModelAttribute("offreForm") OffreForm offreForm,
                              BindingResult formValidation,
                              Model model,
                              Principal principal) {
        if (formValidation.hasErrors()) {
            return "createOffre";
        }

        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();

        OffreResponse offre = offreService.saveOffre(offreForm, StatutOffre.DRAFT, userService.getUserProfile(email));

        model.addAttribute("offredetails", offre);
        return "redirect:/offres/" + offre.getId();
    }

    @GetMapping("/offres/{id}/editer")
    @RolesAllowed("ROLE_ADMIN")
    public String showEditFormOffre(@PathVariable(value = "id") Long idOffre,
                                    Model model) {
        OffreForm offreForm = offreService.getOffreFormbyId(idOffre);
        model.addAttribute("offreForm", offreForm);
        model.addAttribute("idOffre", idOffre);
        return "editOffre";
    }

    @GetMapping("/offres/{id}/supprimer")
    @RolesAllowed("ROLE_ADMIN")
    public String deleteOffre(@PathVariable(value = "id") Long idOffre) {
        offreService.deleteOffre(idOffre);
        return "redirect:/offres";
    }

    @GetMapping("/offres/{id}/accepter")
    @RolesAllowed("ROLE_ADMIN")
    public String acceptOffre(@PathVariable(value = "id") Long idOffre) {
        offreService.setOffreAccepted(idOffre);
        return "redirect:/offres/" + idOffre;
    }

    @PostMapping("/offres/editer")
    @RolesAllowed("ROLE_ADMIN")
    public String updateOffre(@Valid @ModelAttribute("offreForm") OffreForm offreForm,
                              @RequestParam("id_offre") Long idOffre,
                              BindingResult formValidation,
                              Model model) {
        if (formValidation.hasErrors()) {
            return "editOffre";
        }

        OffreResponse offre = offreService.updateOffre(offreForm, idOffre);
        model.addAttribute("offredetails", offre);
        return "redirect:/offres/" + offre.getId();
    }
}