package io.ekip.implicactioncore.configuration.security;

import io.ekip.implicactioncore.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class RegisterFilter extends GenericFilterBean {

    @Autowired
    UserService userService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = ((HttpServletRequest) request);
        HttpServletResponse httpResponse = ((HttpServletResponse) response);

        if (!httpRequest.getRequestURI().contains("/logout")) {
            if (!httpRequest.getRequestURI().contains("/assets/")) {

                KeycloakSecurityContext keycloakPrincipal = getKeycloakPrincipal();

                if (keycloakPrincipal != null) {
                    String email = keycloakPrincipal.getToken().getEmail();
                    boolean isAdmin = keycloakPrincipal.getToken().getRealmAccess().getRoles().contains("ROLE_ADMIN");
                    if (!userService.userIsRegistered(email) && !"/register".equals(httpRequest.getRequestURI())) {
                        httpResponse.sendRedirect("/register");
                        return;
                    } else if (!userService.userIsApproved(email)
                            && userService.userIsRegistered(email)
                            && !"/waitingApproval".equals(httpRequest.getRequestURI())
                            && !isAdmin
                    ) {
                        httpResponse.sendRedirect("/waitingApproval");
                        return;
                    }
                }
            }
        }
            chain.doFilter(request, httpResponse);
    }
    private KeycloakSecurityContext getKeycloakPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null) {
            Object principal = authentication.getPrincipal();

            if (principal instanceof KeycloakPrincipal) {
                return KeycloakPrincipal.class.cast(principal).getKeycloakSecurityContext();
            }
        }

        return null;
    }

}
