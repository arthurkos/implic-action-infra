package io.ekip.implicactioncore.service;

import io.ekip.implicactioncore.adapter.OffreMapper;
import io.ekip.implicactioncore.dto.OffreResponse;
import io.ekip.implicactioncore.dto.UserResponse;
import io.ekip.implicactioncore.dto.exception.ImplicRuntimeException;
import io.ekip.implicactioncore.dto.form.OffreForm;
import io.ekip.implicactioncore.model.OffreEntity;
import io.ekip.implicactioncore.model.StatutOffre;
import io.ekip.implicactioncore.repository.OffreRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class OffreService {

    private final OffreRepository offreRepository;

    public OffreResponse saveOffre(OffreForm offreForm, StatutOffre statutOffre, UserResponse user) {
        OffreEntity offre = OffreMapper.entityFromForm(offreForm, user.getId(), LocalDate.now(), statutOffre);
        OffreEntity offreEntity = offreRepository.save(offre);
        return OffreMapper.responseFromEntity(offreEntity);
    }

    public List<OffreResponse> getAllOffre() {
        return offreRepository.findAll().stream()
                .map(OffreMapper::responseFromEntity)
                .collect(Collectors.toList());
    }

    public OffreResponse getOffreById(Long id) {
        Optional<OffreEntity> maybeOffre = offreRepository.findById(id);
        return maybeOffre
                .map(OffreMapper::responseFromEntity)
                .orElseThrow(() -> new ImplicRuntimeException("Aucune offe n'existe pour cet id: " + id));
    }

    public void deleteOffre(Long id) {
        Optional<OffreEntity> maybeOffre = offreRepository.findById(id);
        maybeOffre.ifPresent(offreRepository::delete);
    }

    public void setOffreAccepted(Long id) {
        Optional<OffreEntity> maybeOffre = offreRepository.findById(id);
        maybeOffre.ifPresent(offre -> {
            offre.setStatut(StatutOffre.ACCEPTED);
            offreRepository.save(offre);
        });
    }

    public OffreForm getOffreFormbyId(Long idOffre) {
        Optional<OffreEntity> maybeOffre = offreRepository.findById(idOffre);
        return maybeOffre
                .map(OffreMapper::formFromEntity)
                .orElseThrow(() -> new ImplicRuntimeException("Aucune offe n'existe pour cet id: " + idOffre));
    }

    public OffreResponse updateOffre(OffreForm offreForm, Long idOffre) {
        OffreEntity offreToUpdate;

        if (offreRepository.existsById(idOffre)) {
            offreToUpdate = offreRepository.findOffreEntityById(idOffre);

            OffreEntity offreUpdated = OffreMapper.entityFromForm(offreForm, offreToUpdate.getUser(), LocalDate.now(), StatutOffre.DRAFT);

            offreToUpdate.setId(idOffre);
            offreToUpdate.setUser(offreUpdated.getUser());
            offreToUpdate.setDatePublication(offreUpdated.getDatePublication());
            offreToUpdate.setTitrePoste(offreUpdated.getTitrePoste());
            offreToUpdate.setTypeContrat(offreUpdated.getTypeContrat());
            offreToUpdate.setDescription(offreUpdated.getDescription());
            offreToUpdate.setNomEntreprise(offreUpdated.getNomEntreprise());
            offreToUpdate.setUrlEntreprise(offreUpdated.getUrlEntreprise());
            offreToUpdate.setDescEntreprise(offreUpdated.getDescEntreprise());
            offreToUpdate.setUrlVideo(offreUpdated.getUrlVideo());
            offreToUpdate.setStatut(offreUpdated.getStatut());
            offreToUpdate.setTwitter(offreUpdated.getTwitter());
            offreToUpdate.setLieuPoste(offreUpdated.getLieuPoste());
            offreRepository.save(offreToUpdate);
            return OffreMapper.responseFromEntity(offreToUpdate);

        }
        return null;
    }
}
