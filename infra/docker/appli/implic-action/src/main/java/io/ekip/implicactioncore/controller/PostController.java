package io.ekip.implicactioncore.controller;

import io.ekip.implicactioncore.dto.GroupResponse;
import io.ekip.implicactioncore.dto.PostResponse;
import io.ekip.implicactioncore.dto.UserResponse;
import io.ekip.implicactioncore.dto.form.PostForm;
import io.ekip.implicactioncore.dto.form.ReplyForm;
import io.ekip.implicactioncore.service.PostService;
import io.ekip.implicactioncore.service.GroupService;
import io.ekip.implicactioncore.service.UserService;
import io.ekip.implicactioncore.service.ReplyService;
import io.ekip.implicactioncore.service.InGroupService;
import lombok.AllArgsConstructor;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@AllArgsConstructor
public class PostController {
    PostService postService;
    GroupService groupService;
    ReplyService replyService;
    UserService userService;
    InGroupService inGroupService;


    @GetMapping("/posts/creer")
    public String showCreerPost(PostForm postForm) {
        return "infosGroup";
    }

    @PostMapping("/posts/creer")
    public String creerPost(@Valid @ModelAttribute("postForm") PostForm postForm,
                            BindingResult formValidation,
                            RedirectAttributes attr,
                            @RequestParam("id_group") Long groupId,
                            Principal principal) {
        if (formValidation.hasErrors()) {
            attr.addFlashAttribute("org.springframework.validation.BindingResult.postForm", formValidation);
            attr.addFlashAttribute("postForm", postForm);
            return "redirect:/groupes/" + groupId;
        }
        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();
        PostResponse postResponse = postService.savePost(postForm, groupId, email);

        return "redirect:/groupes/" + postResponse.getGroupe().getId();
    }

    @GetMapping("/groupes/{groupId}/posts/{id}")
    public String detailsPost(Model model,
                              @PathVariable(value = "groupId") Long idGroup,
                              @PathVariable(value = "id") Long idPost,
                              Principal principal) {
        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();

        UserResponse user = userService.getUserProfile(email);
        GroupResponse groupResponse = groupService.getGroupById(idGroup);

        boolean isSubscribe = inGroupService.isSubscribeGroup(idGroup, user.getId());
        boolean isModo = inGroupService.isModo(idGroup, user);
        List<UserResponse> listSubscribedUsers = groupService.getUsersFromGroupId(idGroup);
        model.addAttribute("groupdetails", groupResponse);
        model.addAttribute("idPost", idPost);
        model.addAttribute("isSubscribeGroup", isSubscribe);
        model.addAttribute("listUsers", listSubscribedUsers);
        model.addAttribute("isModo", isModo);

        if (!model.containsAttribute("postForm")) {
            model.addAttribute("postForm", new PostForm(null, null));
        }
        groupResponse.getPostList().forEach(post -> {
            if (!model.containsAttribute("replyForm" + post.getId())) {
                model.addAttribute("replyForm" + post.getId(), new ReplyForm(null));
            }
        });

        return "infosGroup";
    }

}
