package io.ekip.implicactioncore.dto;

import lombok.Getter;
import lombok.Value;

import java.time.LocalDate;
import java.util.List;

@Value
@Getter
public class UserResponse {
    Long id;
    String email;
    String nomPrenom;
    LocalDate birthdate;
    Genre genre;
    String telephone;
    String adresse;
    String ville;
    String codePostal;
    String departement;
    Region region;
    Visibility visibility;
    String pays;
    String presentation;
    String objectifPro;
    String apportReseau;
    String attenteReseau;
    List<String> armeeOrigine;
    List<String> corpsStatutaire;
    List<LocalDate> dateExperience;
    List<String> descriptionExperience;
    List<LocalDate> dateFormation;
    List<String> descriptionFormation;
    List<String> interets;
    String descriptionEmploi;
    List<String> typeEmploi;
    String citation;
    List<GroupResponse> groups;
    Boolean isApproved;
}
