package io.ekip.implicactioncore.dto.form.validator;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MatchValidator implements ConstraintValidator<ValuesMatch, Object> {

    private String firstValue;
    private String secondValue;

    @Override
    public void initialize(ValuesMatch constraintAnnotation) {
        this.firstValue = constraintAnnotation.firstValue();
        this.secondValue = constraintAnnotation.secondValue();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        Object fieldValue = new BeanWrapperImpl(value)
                .getPropertyValue(firstValue);
        Object fieldMatchValue = new BeanWrapperImpl(value)
                .getPropertyValue(secondValue);

        if (fieldValue != null) {
            return fieldValue.equals(fieldMatchValue);
        } else {
            return fieldMatchValue == null;
        }
    }
}
