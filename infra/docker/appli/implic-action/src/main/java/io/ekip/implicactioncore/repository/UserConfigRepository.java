package io.ekip.implicactioncore.repository;

import io.ekip.implicactioncore.model.UserConfigEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserConfigRepository extends JpaRepository<UserConfigEntity, Long> {

}
