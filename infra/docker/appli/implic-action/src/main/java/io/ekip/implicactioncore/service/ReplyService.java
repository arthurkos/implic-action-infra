package io.ekip.implicactioncore.service;

import io.ekip.implicactioncore.adapter.ReplyMapper;
import io.ekip.implicactioncore.dto.ReplyResponse;
import io.ekip.implicactioncore.dto.exception.ImplicRuntimeException;
import io.ekip.implicactioncore.dto.form.ReplyForm;
import io.ekip.implicactioncore.model.PostEntity;
import io.ekip.implicactioncore.model.ReplyEntity;
import io.ekip.implicactioncore.model.UserEntity;
import io.ekip.implicactioncore.repository.PostRepository;
import io.ekip.implicactioncore.repository.ReplyRepository;
import io.ekip.implicactioncore.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ReplyService {
    ReplyRepository replyRepository;
    PostRepository postRepository;
    UserRepository userRepository;

    public ReplyResponse saveReply(ReplyForm replyForm, Long postId, String userEmail) {
        Optional<UserEntity> maybeUser = userRepository.getByEmail(userEmail);
        UserEntity userEntity = maybeUser.get();

        Optional<PostEntity> maybePost = postRepository.findById(postId);

        return maybePost.map(post -> {
            ReplyEntity replyEntity = ReplyMapper.entityFromForm(replyForm, post, userEntity);
            replyRepository.save(replyEntity);
            return ReplyMapper.responseFromEntity(replyEntity);
        }).orElseThrow(() -> new ImplicRuntimeException("Aucun post n'existe pour cet id: " + postId));
    }

    public ReplyResponse getReplyById(Long id) {
        Optional<ReplyEntity> maybeGroup = replyRepository.findById(id);
        return maybeGroup
                .map(ReplyMapper::responseFromEntity)
                .orElseThrow(() -> new ImplicRuntimeException("Aucun groupe n'existe pour cet id: " + id));
    }

}
