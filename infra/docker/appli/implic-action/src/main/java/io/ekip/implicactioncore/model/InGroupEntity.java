package io.ekip.implicactioncore.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "in_group")
@Data
@IdClass(InGroupKey.class)
public class InGroupEntity {
    @Id
    @Column(name = "ref_user")
    private Long userId;

    @Id
    @Column(name = "ref_group")
    private Long groupId;
    @Column(name = "is_modo")
    private Boolean isModo;


}
