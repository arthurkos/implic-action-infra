package io.ekip.implicactioncore.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Region {

    //Region code (Code ISO 3166-2)
    FR_ARA("FR_ARA"),
    FR_BFC("FR_BFC"),
    FR_BRE("FR_BRE"),
    FR_CVL("FR_CVL"),
    FR_COR("FR_COR"),
    FR_GES("FR_GES"),
    FR_HDF("FR_HDF"),
    FR_IDF("FR_IDF"),
    FR_NOR("FR_NOR"),
    FR_NAQ("FR_NAQ"),
    FR_OCC("FR_OCC"),
    FR_PDL("FR_PDL"),
    FR_PAC("FR_PAC"),
    NONE("NONE"),
    ;

    private final String label;

}
