package io.ekip.implicactioncore.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Genre implements ImplicBaseEnum<String>{

    FEMME("femme", "Femme"),
    HOMME("homme", "Homme"),
    AUTRE("autre", "Autre")
    ;

    private final String value;
    private final String label;

}
