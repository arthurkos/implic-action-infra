package io.ekip.implicactioncore.dto.form;

import io.ekip.implicactioncore.dto.Genre;
import io.ekip.implicactioncore.dto.Region;
import io.ekip.implicactioncore.dto.form.validator.EnumConstraint;
import lombok.Value;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Value
public class GroupForm {
    @NotEmpty
    String name;

    @NotEmpty
    String desc;

    @EnumConstraint(enumClass = Region.class)
    String region;

}
