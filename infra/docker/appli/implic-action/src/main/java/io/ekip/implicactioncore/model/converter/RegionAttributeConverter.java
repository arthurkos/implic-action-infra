package io.ekip.implicactioncore.model.converter;

import io.ekip.implicactioncore.model.Region;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RegionAttributeConverter implements AttributeConverter<Region, String> {
    @Override
    public String convertToDatabaseColumn(Region region) {
        if(region == null) {
            return null;
        }
        return region.getLabel();
    }

    @Override
    public Region convertToEntityAttribute(String rawRegion) {
        if(rawRegion == null) {
            return null;
        }
        return Region.valueOf(rawRegion.toUpperCase());
    }
}
