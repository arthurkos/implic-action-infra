package io.ekip.implicactioncore.model.converter;

import io.ekip.implicactioncore.model.Visibility;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class VisibilityAttributeConverter implements AttributeConverter<Visibility, String> {
    @Override
    public String convertToDatabaseColumn(Visibility visibility) {
        if(visibility == null) {
            return null;
        }
        return visibility.getLabel();
    }

    @Override
    public Visibility convertToEntityAttribute(String rawVisibility) {
        if(rawVisibility == null) {
            return null;
        }
        return Visibility.fromValue(rawVisibility.toLowerCase());
    }
}
