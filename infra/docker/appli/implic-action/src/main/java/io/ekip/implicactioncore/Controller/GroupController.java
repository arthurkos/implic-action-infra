package io.ekip.implicactioncore.controller;

import io.ekip.implicactioncore.dto.GroupResponse;
import io.ekip.implicactioncore.dto.UserResponse;
import io.ekip.implicactioncore.dto.form.GroupForm;
import io.ekip.implicactioncore.dto.form.PostForm;
import io.ekip.implicactioncore.dto.form.ReplyForm;
import io.ekip.implicactioncore.service.*;
import lombok.AllArgsConstructor;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.representations.AccessToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@AllArgsConstructor
public class GroupController {
    GroupService groupService;
    PostService postService;
    ReplyService replyService;
    UserService userService;
    InGroupService inGroupService;

    @GetMapping("/groupes/creer")
    @RolesAllowed("ROLE_ADMIN")
    public String showCreateGroup(GroupForm groupForm) {
        return "createGroup";
    }

    @PostMapping("/groupes/creer")
    @RolesAllowed("ROLE_ADMIN")
    public String createGroup(@Valid @ModelAttribute("groupForm") GroupForm groupForm,
                              BindingResult formValidation,
                              Model model) {
        if (formValidation.hasErrors()) {
            return "createGroup";
        }
        GroupResponse groupResponse = groupService.saveGroup(groupForm);
        model.addAttribute("groupdetail", groupResponse);

        return "redirect:/groupes/" + groupResponse.getId();
    }

    @GetMapping("/groupes")
    public String listOffer(Model model,
                            @RequestParam("region") Optional<String> maybeRegion) {
        List<GroupResponse> groups = groupService.getAllGroup().stream()
                .filter(group -> maybeRegion.map(region -> group.getRegion().getLabel().equalsIgnoreCase(region)).orElse(true))
                .collect(Collectors.toList());

        model.addAttribute("listGroups", groups);
        return "listGroups";
    }

    @GetMapping("/groupes/{id}")
    public String detailsGroup(Model model,
                               @PathVariable(value = "id") Long idGroup,
                               Principal principal,
                               Long idPost) {
        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();

        UserResponse user = userService.getUserProfile(email);
        GroupResponse groupResponse = groupService.getGroupById(idGroup);

        boolean isSubscribe = inGroupService.isSubscribeGroup(idGroup, user.getId());
        boolean isModo = inGroupService.isModo(idGroup, user) || accessToken.getRealmAccess().getRoles().contains("ROLE_ADMIN");
        List<UserResponse> listSubscribedUsers = groupService.getUsersFromGroupId(idGroup);
        model.addAttribute("groupdetails", groupResponse);
        model.addAttribute("idPost", idPost);
        model.addAttribute("isSubscribeGroup", isSubscribe);
        model.addAttribute("listUsers", listSubscribedUsers);
        model.addAttribute("isModo", isModo);

        if (!model.containsAttribute("postForm")) {
            model.addAttribute("postForm", new PostForm(null, null));
        }
        groupResponse.getPostList().forEach(post -> {
            if (!model.containsAttribute("replyForm" + post.getId())) {
                model.addAttribute("replyForm" + post.getId(), new ReplyForm(null));
            }
        });

        return "infosGroup";
    }

    @GetMapping("/groupes/{id}/editer")
    public String showEditFormGroup(@PathVariable(value = "id") Long idGroup,
                                    Model model, Principal principal) {
        KeycloakAuthenticationToken keycloakAuthenticationToken = (KeycloakAuthenticationToken) principal;
        AccessToken accessToken = keycloakAuthenticationToken.getAccount().getKeycloakSecurityContext().getToken();
        String email = accessToken.getEmail();
        UserResponse user = userService.getUserProfile(email);
        if (inGroupService.isModo(idGroup, user) || accessToken.getRealmAccess().getRoles().contains("ROLE_ADMIN")) {
            GroupForm groupForm = groupService.getGroupFormbyId(idGroup);
            model.addAttribute("groupForm", groupForm);
            model.addAttribute("idGroup", idGroup);
            return "editGroup";
        }
        return "redirect:/groupes/" + idGroup;
    }

    @PostMapping("/groupes/editer")
    public String updateOffre(@Valid @ModelAttribute("groupForm") GroupForm groupForm,
                              @RequestParam("id_group") Long idGroup,
                              BindingResult formValidation,
                              Model model) {
        if (formValidation.hasErrors()) {
            return "editGroup";
        }

        GroupResponse groupResponse = groupService.updateGroup(groupForm, idGroup);

        model.addAttribute("groupdetails", groupResponse);
        return "redirect:/groupes/" + groupResponse.getId();
    }

    @GetMapping("/groupes/{id}/supprimer")
    @RolesAllowed("ROLE_ADMIN")
    public String adminDeleteGroup(@PathVariable(value = "id") Long idGroup) {
        groupService.delete(idGroup);
        return "redirect:/administration/groupes";
    }

}
