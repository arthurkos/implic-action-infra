package io.ekip.implicactioncore.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ImplicArraysUtils {

    public static boolean allElementsAreNull(Object[] arr){
        for (Object obj: arr) {
            if(obj != null){
                return false;
            }
        }
        return true;
    }
}