package io.ekip.implicactioncore.repository;

import io.ekip.implicactioncore.model.OffreEntity;
import io.ekip.implicactioncore.model.StatutOffre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OffreRepository extends JpaRepository<OffreEntity, Long> {
    List<OffreEntity> findByStatut(StatutOffre statut);

    OffreEntity findOffreEntityById(Long idOffre);

    void deleteById(Long idOffre);
}
