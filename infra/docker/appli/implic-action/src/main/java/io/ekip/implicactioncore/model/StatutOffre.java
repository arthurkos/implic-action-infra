package io.ekip.implicactioncore.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatutOffre {

    DRAFT("draft"),
    PENDING("pending"),
    ACCEPTED("accepted"),
    ;

    private final String label;

}
