package io.ekip.implicactioncore.dto.form;

import lombok.Value;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Value
public class ReplyForm {

    @NotEmpty
    @Size(max = 1000)
    String message;

}
