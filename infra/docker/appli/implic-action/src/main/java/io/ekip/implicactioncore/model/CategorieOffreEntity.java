package io.ekip.implicactioncore.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "categorie_offre")
public class CategorieOffreEntity implements Serializable {

    @Id
    @ManyToOne
    @JoinColumn(name = "ref_offre")
    private OffreEntity offre;

    @Id
    @ManyToOne
    @JoinColumn(name = "ref_categorie")
    private CategorieEmploiEntity categorieEmploi;

    public CategorieOffreEntity() {
    }

    public CategorieOffreEntity(OffreEntity offre, CategorieEmploiEntity categorieEmploi) {
        this.offre = offre;
        this.categorieEmploi = categorieEmploi;
    }
}
