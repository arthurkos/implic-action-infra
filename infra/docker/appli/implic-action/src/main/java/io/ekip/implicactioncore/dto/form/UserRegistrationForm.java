package io.ekip.implicactioncore.dto.form;

import io.ekip.implicactioncore.dto.Genre;
import io.ekip.implicactioncore.dto.Region;
import io.ekip.implicactioncore.dto.Visibility;
import io.ekip.implicactioncore.dto.form.validator.EnumConstraint;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Value
public class UserRegistrationForm {

    @NotNull
    @EnumConstraint(enumClass = Visibility.class)
    String visibiliteProfile;

    @NotNull
    @EnumConstraint(enumClass = Genre.class)
    String genre;

    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate birthdate;

    @NotNull
    @NotEmpty
    String telephone;

    @NotNull
    @NotEmpty
    String adresse;

    @NotNull
    @EnumConstraint(enumClass = Region.class)
    String region;

    @NotNull
    @NotEmpty
    String presentation;

    @NotNull
    @NotEmpty
    String objectifPro;

    @NotNull
    @NotEmpty
    String apportReseau;

    @NotNull
    @NotEmpty
    String attenteReseau;

}
