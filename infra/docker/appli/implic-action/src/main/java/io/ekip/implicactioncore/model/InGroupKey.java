package io.ekip.implicactioncore.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InGroupKey implements Serializable {
    private Long userId;
    private Long groupId;
}
