alter table post add COLUMN ref_user bigserial not null;

alter table post add constraint post_ref_user_fkey foreign key (ref_user) references implic_user (id);
