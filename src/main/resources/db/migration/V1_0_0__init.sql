CREATE TABLE "user"
(
    "id"                         bigserial PRIMARY KEY,
    "username"                   text NOT NULL UNIQUE,
    "email"                      text NOT NULL,
    "nom_prenom"                 text NOT NULL,
    "genre"                      bool NOT NULL,
    "anniversaire"               DATE NOT NULL,
    "telephone"                  text NOT NULL,
    "adresse"                    text NOT NULL,
    "code_postal"                text,
    "ville"                      text,
    "departement"                text,
    "region"                     text,
    "pays"                       text,
    "presentation"               text NOT NULL,
    "objectif_pro"               text NOT NULL,
    "apport_reseau"              text NOT NULL,
    "attente_reseau"             text NOT NULL,
    "armee_origine"              text[] NOT NULL,
    "corps_statutaire"           text[] NOT NULL,
    "date_experience"            DATE[] NOT NULL,
    "description_experience"     text[] NOT NULL,
    "date_formation"             DATE[] NOT NULL,
    "description_formation"      text[] NOT NULL,
    "interets"                   text[],
    "description_emploi"         text,
    "type_emploi"                text[],
    "citation"                   text,

    "date_inscription"           DATE
    /*missing url for profil picture (add it later)*/
);

CREATE TYPE visibility AS ENUM
(
    'private',
    'friends',
    'all',
    'members'
);

CREATE TABLE "user_config"
(
    "ref_user"                              bigserial REFERENCES "user"(id) NOT NULL,
    "visibility_email"                      visibility DEFAULT 'all',
    "visibility_nom_prenom"                 visibility DEFAULT 'all',
    "visibility_genre"                      visibility DEFAULT 'all',
    "visibility_anniversaire"               visibility DEFAULT 'friends',
    "visibility_telephone"                  visibility DEFAULT 'all',
    "visibility_adresse"                    visibility DEFAULT 'friends',
    "visibility_code_postal"                visibility DEFAULT 'friends',
    "visibility_ville"                      visibility DEFAULT 'friends',
    "visibility_departement"                visibility DEFAULT 'friends',
    "visibility_region"                     visibility DEFAULT 'members',
    "visibility_pays"                       visibility DEFAULT 'members',
    "visibility_presentation"               visibility DEFAULT 'all',
    "visibility_objectif_pro"               visibility DEFAULT 'all',
    "visibility_apport_reseau"              visibility DEFAULT 'all',
    "visibility_attente_reseau"             visibility DEFAULT 'all',
    "visibility_armee_origine"              visibility DEFAULT 'all',
    "visibility_corps_statutaire"           visibility DEFAULT 'all',
    "visibility_experience"                 visibility DEFAULT 'all',
    "visibility_formation"                  visibility DEFAULT 'all',
    "visibility_interets"                   visibility DEFAULT 'all',
    "visibility_description_emploi"         visibility DEFAULT 'all',
    "visibility_type_emploi"                visibility DEFAULT 'all',
    "visibility_citation"                   visibility DEFAULT 'all',
    
    "last_cgu_accepted"                     bool NOT NULL,
    "is_active"                             bool DEFAULT true
);

CREATE TYPE "type_contrat" AS ENUM
(
    'cdi',
    'cdd',
    'freelance',
    'stage',
    'temps_partiel'
);

CREATE TABLE "categorie_emploi"
(
    "id"                         bigserial PRIMARY KEY,
    "nom_categorie"              text NOT NULL
);

CREATE TYPE "statut_offre" AS ENUM
(
    'draft',
    'pending',
    'accepted'
);

CREATE TABLE "offre"
(  
    "id"                         bigserial PRIMARY KEY,
    "ref_user"                   bigserial REFERENCES "user"(id) NOT NULL,
    "date_publication"           DATE, /*set when offer is accepted*/
    "titre_poste"                text NOT NULL, /*nom du poste*/
    "type_poste"                 type_contrat NOT NULL,
    /*"ref_categorie"              bigserial REFERENCES categorie_poste(id),*/
    "description"                text NOT NULL,
    "email"                      text NOT NULL,
    "nom_entreprise"             text NOT NULL,
    "url_entreprise"             text,
    "description_entreprise"     VARCHAR(150),
    "url_video"                  VARCHAR(250),
    "twitter"                    VARCHAR(30),
    /*missing url for company logo (add it later)*/
    "statut"                     statut_offre NOT NULL                  
);

CREATE TABLE "categorie_offre"
(
    "ref_offre"                 bigserial REFERENCES "offre"(id) NOT NULL,
    "ref_categorie"             bigserial REFERENCES "categorie_emploi"(id) NOT NULL
);
