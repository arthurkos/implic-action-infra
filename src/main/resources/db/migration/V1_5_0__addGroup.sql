CREATE TABLE groupe(
    "id"                bigserial PRIMARY KEY,
    "name"              VARCHAR (150) NOT NULL,
    "desc_group"              VARCHAR (500) NOT NULL,
    "region"            VARCHAR (1000)
);

CREATE TABLE post(
    "id"                bigserial PRIMARY KEY,
    "name"              VARCHAR (150) NOT NULL,
    "message"           VARCHAR (1000) NOT NULL,
    "creation_date"     DATE  NOT NULL,
    "update_date"       DATE  NOT NULL,
    "ref_groupe"        bigserial REFERENCES "groupe"(id) NOT NULL
);

CREATE TABLE post_reply(
    "id"                bigserial PRIMARY KEY,
    "message"           VARCHAR (1000) NOT NULL,
    "ref_post"          bigserial REFERENCES "post"(id) NOT NULL,
    "ref_user"          bigserial REFERENCES "implic_user"(id) NOT NULL
);

CREATE TABLE sub_reply(
    "id"                bigserial PRIMARY KEY,
    "message"           VARCHAR (1000) NOT NULL,
    "ref_user"          bigserial REFERENCES "implic_user"(id) NOT NULL,
    "ref_post_reply"         bigserial REFERENCES "post_reply"(id) NOT NULL
);

CREATE TABLE in_group(
    "ref_group"         bigserial  REFERENCES "groupe"(id) NOT NULL,
    "ref_user"          bigserial  REFERENCES "implic_user"(id) NOT NULL,
    "is_modo"           BOOL  NOT NULL
);