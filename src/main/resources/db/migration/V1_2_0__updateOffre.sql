ALTER TABLE Offre
ADD COLUMN "lieu_poste" text;

ALTER TABLE offre
    ALTER COLUMN type_poste type Text ;

ALTER TABLE offre
    ALTER COLUMN statut type Text  ;

DROP TYPE statut_offre;
DROP TYPE type_contrat;

