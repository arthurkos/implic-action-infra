package io.ekip.implicactioncore.repository;

import io.ekip.implicactioncore.model.ReplyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReplyRepository extends JpaRepository<ReplyEntity, Long> {

    ReplyEntity findReplyEntityById(Long idReply);
}
