package io.ekip.implicactioncore.dto;

import io.ekip.implicactioncore.model.GroupEntity;
import io.ekip.implicactioncore.model.UserEntity;
import lombok.Value;

import java.time.LocalDate;
import java.util.List;


@Value
public class PostResponse {
    Long id;
    String name;
    String message;
    LocalDate creationDate;
    LocalDate updateDate;
    GroupEntity groupe;
    UserEntity user;
    List<ReplyResponse> replyList;
}
