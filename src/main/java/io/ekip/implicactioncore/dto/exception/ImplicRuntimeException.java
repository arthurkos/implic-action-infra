package io.ekip.implicactioncore.dto.exception;

public class ImplicRuntimeException extends RuntimeException{

    public ImplicRuntimeException(String message) {
        super(message);
    }
}
