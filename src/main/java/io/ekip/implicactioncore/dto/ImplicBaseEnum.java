package io.ekip.implicactioncore.dto;

public interface ImplicBaseEnum<T> {

    T getValue();

}
