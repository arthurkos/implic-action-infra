package io.ekip.implicactioncore.dto.form;

import io.ekip.implicactioncore.dto.Genre;
import io.ekip.implicactioncore.dto.Visibility;
import io.ekip.implicactioncore.dto.form.validator.EnumConstraint;
import io.ekip.implicactioncore.model.Region;
import lombok.Value;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Value
public class UserEditProfileForm {

    @NotNull
    @EnumConstraint(enumClass = Genre.class)
    String genre;
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate birthdate;
    @NotNull
    @NotEmpty
    String telephone;

    @NotNull
    @NotEmpty
    String adresse;

    String ville;
    String codePostal;
    String departement;
    Region region;

    @EnumConstraint(enumClass = Visibility.class)
    String visibiliteProfile;

    String pays;
    List<String> armeeOrigine;
    List<String> corpsStatutaire;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate[] dateExperience;

    List<String> descriptionExperience;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate[] dateFormation;

    List<String> descriptionFormation;
    List<String> interets;
    String descriptionEmploi;
    List<String> typeEmploi;
    String citation;

    @NotNull
    @NotEmpty
    String presentation;
    @NotNull
    @NotEmpty
    String objectifPro;
    @NotNull
    @NotEmpty
    String apportReseau;
    @NotNull
    @NotEmpty
    String attenteReseau;

}
