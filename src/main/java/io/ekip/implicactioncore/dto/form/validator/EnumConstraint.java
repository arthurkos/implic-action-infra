package io.ekip.implicactioncore.dto.form.validator;

import io.ekip.implicactioncore.dto.ImplicBaseEnum;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = EnumValidator.class)
public @interface EnumConstraint {
    Class<? extends ImplicBaseEnum<String>> enumClass();
    String message() default "mauvaise valeur selectionée";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
