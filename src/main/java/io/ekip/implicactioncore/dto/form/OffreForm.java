package io.ekip.implicactioncore.dto.form;

import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Value
public class OffreForm {

    @NotEmpty
    String titrePoste;

    @NotEmpty
    String typePoste;

    @NotEmpty
    String description;

    @NotEmpty
    @Email
    String email;

    @NotEmpty
    String nomEntreprise;

    String urlEntreprise;

    String descriptionEntreprise;

    String urlVideo;

    String urlTwitter;

    String lieuPoste;

}