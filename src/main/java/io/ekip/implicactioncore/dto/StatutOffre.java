package io.ekip.implicactioncore.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum StatutOffre {

    DRAFT("draft", "Brouillons"),
    PENDING("pending", "En attente"),
    ACCEPTED("accepted", "Acceptée");

    private final String value;
    private final String label;

}
