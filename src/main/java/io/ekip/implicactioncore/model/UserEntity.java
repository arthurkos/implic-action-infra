package io.ekip.implicactioncore.model;

import com.vladmihalcea.hibernate.type.array.ListArrayType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@TypeDef(
        name = "list-array",
        typeClass = ListArrayType.class
)

@Entity
@Table(name = "implic_user")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ref_config", referencedColumnName = "id")
    private UserConfigEntity userConfigEntity;

    @Column(name = "email")
    private String email;

    @Column(name = "nom_prenom")
    private String nomPrenom;

    @Column(name = "genre")
    private Genre genre;

    @Column(name = "anniversaire")
    private LocalDate birthdate;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "ville")
    private String ville;

    @Column(name = "code_postal")
    private String codePostal;

    @Column(name = "departement")
    private String departement;

    @Column(name = "region")
    private Region region;

    @Column(name = "pays")
    private String pays;

    @Column(name = "presentation")
    private String presentation;

    @Column(name = "objectif_pro")
    private String objectifPro;

    @Column(name = "apport_reseau")
    private String apportReseau;

    @Column(name = "attente_reseau")
    private String attenteReseau;

    @Column(name = "armee_origine")
    @Type(type = "list-array")
    private List<String> armeeOrigine;

    @Column(name = "corps_statutaire")
    @Type(type = "list-array")
    private List<String> corpsStatutaire;

    @Column(name = "date_experience")
    @Type(type = "list-array")
    private List<Date> dateExperience;

    @Column(name = "description_experience")
    @Type(type = "list-array")
    private List<String> descriptionExperience;

    @Column(name = "date_formation")
    @Type(type = "list-array")
    private List<Date> dateFormation;

    @Column(name = "description_formation")
    @Type(type = "list-array")
    private List<String> descriptionFormation;

    @Column(name = "interets")
    @Type(type = "list-array")
    private List<String> interets;

    @Column(name = "description_emploi")
    private String descriptionEmploi;

    @Column(name = "type_emploi")
    @Type(type = "list-array")
    private List<String> typeEmploi;

    @Column(name = "citation")
    private String citation;

    @Column(name = "date_inscription")
    private LocalDate dateInscription;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<PostEntity> post;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<ReplyEntity> reply;

    public List<LocalDate> getDateExperience() {
        return dateExperience.stream()
                .map(Date::toLocalDate)
                .collect(Collectors.toList());
    }

    public List<LocalDate> getDateFormation() {
        return dateFormation.stream()
                .map(Date::toLocalDate)
                .collect(Collectors.toList());
    }

}
