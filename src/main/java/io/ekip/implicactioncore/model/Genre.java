package io.ekip.implicactioncore.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Genre {

    FEMME("femme"),
    HOMME("homme"),
    AUTRE("autre")
    ;

    private final String label;

}
