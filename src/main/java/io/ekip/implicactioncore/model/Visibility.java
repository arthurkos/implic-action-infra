package io.ekip.implicactioncore.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum Visibility {

    PRIVATE("private"),
    FRIENDS("friends"),
    ALL("all"),
    MEMBER("members")
    ;

    private final String label;

    public static Visibility fromDto(io.ekip.implicactioncore.dto.Visibility visibilityDto) {
        return Arrays.stream(values())
                .filter(visibility -> visibility.name().equals(visibilityDto.name()))
                .findFirst()
                .orElse(Visibility.PRIVATE);
    }

    public static Visibility fromValue(String value) {
        return Arrays.stream(values())
                .filter(visibility -> visibility.getLabel().equals(value))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Visibility conversion from string failed for value: " + value));
    }

}
