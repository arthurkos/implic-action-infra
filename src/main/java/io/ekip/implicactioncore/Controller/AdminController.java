package io.ekip.implicactioncore.controller;

import io.ekip.implicactioncore.dto.GroupResponse;
import io.ekip.implicactioncore.dto.OffreResponse;
import io.ekip.implicactioncore.dto.UserResponse;
import io.ekip.implicactioncore.service.GroupService;
import io.ekip.implicactioncore.service.OffreService;
import io.ekip.implicactioncore.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("/administration")
public class AdminController {

    UserService userService;
    GroupService groupService;
    OffreService offreService;

    @GetMapping(value = {"", "/membres"})
    @RolesAllowed("ROLE_ADMIN")
    public String showAdminPageMembres(Model model) {
        List<UserResponse> userList = userService.getAllUser();
        model.addAttribute("users", userList);
        return "adminPageMembres";
    }

    @GetMapping("/groupes")
    @RolesAllowed("ROLE_ADMIN")
    public String showAdminPageGroupes(Model model) {
        List<GroupResponse> groupList = groupService.getAllGroup();
        model.addAttribute("groups", groupList);
        return "adminPageGroupes";
    }

    @GetMapping("/offres")
    @RolesAllowed("ROLE_ADMIN")
    public String showAdminPageOffres(Model model) {
        List<OffreResponse> offreList = offreService.getAllOffre();
        model.addAttribute("offers", offreList);
        return "adminPageOffres";
    }

}
