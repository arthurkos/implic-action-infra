package io.ekip.implicactioncore.adapter;

import io.ekip.implicactioncore.dto.GroupResponse;
import io.ekip.implicactioncore.dto.PostResponse;
import io.ekip.implicactioncore.dto.form.GroupForm;
import io.ekip.implicactioncore.model.GroupEntity;
import io.ekip.implicactioncore.model.Region;
import lombok.experimental.UtilityClass;

import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

@UtilityClass
public class GroupMapper {
    public static GroupEntity entityFromForm(GroupForm groupForm) {
        return new GroupEntity(
                null,
                groupForm.getName(),
                groupForm.getDesc(),
                Region.valueOf(groupForm.getRegion().toUpperCase()),
                Collections.emptyList()
        );
    }

    public static GroupResponse ResponseFromEntity(GroupEntity groupeEntity) {
        return new GroupResponse(
                groupeEntity.getId(),
                groupeEntity.getName(),
                groupeEntity.getDesc(),
                io.ekip.implicactioncore.dto.Region.valueOf(groupeEntity.getRegion().name().toUpperCase()),
                groupeEntity.getPost().stream()
                        .map(PostMapper::responseFromEntity)
                        .sorted(Comparator.comparing(PostResponse::getId).reversed())
                        .collect(Collectors.toList())
        );
    }

    public static GroupForm formFromEntity(GroupEntity groupEntity) {
        return new GroupForm(
                groupEntity.getName(),
                groupEntity.getDesc(),
                groupEntity.getRegion().getLabel()
        );
    }

}
