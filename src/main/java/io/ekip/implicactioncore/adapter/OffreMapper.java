package io.ekip.implicactioncore.adapter;

import io.ekip.implicactioncore.dto.OffreResponse;
import io.ekip.implicactioncore.dto.form.OffreForm;
import io.ekip.implicactioncore.model.OffreEntity;
import io.ekip.implicactioncore.model.StatutOffre;
import io.ekip.implicactioncore.model.TypeContrat;
import lombok.experimental.UtilityClass;

import java.time.LocalDate;

@UtilityClass
public class OffreMapper {

    public static OffreEntity entityFromForm(OffreForm offreForm, Long idUser, LocalDate datePublication, StatutOffre statutOffre) {
        return new OffreEntity(
                null,
                idUser,
                datePublication,
                offreForm.getTitrePoste(),
                TypeContrat.valueOf(io.ekip.implicactioncore.dto.TypeContrat.fromValue(offreForm.getTypePoste()).name()),
                offreForm.getDescription(),
                offreForm.getEmail(),
                offreForm.getNomEntreprise(),
                offreForm.getUrlEntreprise(),
                offreForm.getDescriptionEntreprise(),
                offreForm.getUrlVideo(),
                offreForm.getUrlTwitter(),
                statutOffre,
                offreForm.getLieuPoste()
        );
    }

    public static OffreForm formFromEntity(OffreEntity offreEntity) {
        return new OffreForm(
                offreEntity.getTitrePoste(),
                offreEntity.getTypeContrat().getLabel(),
                offreEntity.getDescription(),
                offreEntity.getEmail(),
                offreEntity.getNomEntreprise(),
                offreEntity.getUrlEntreprise(),
                offreEntity.getDescEntreprise(),
                offreEntity.getUrlVideo(),
                offreEntity.getTwitter(),
                offreEntity.getLieuPoste()
        );
    }

    public static OffreResponse responseFromEntity(OffreEntity offreEntity) {
        return new OffreResponse(
                offreEntity.getId(),
                offreEntity.getDatePublication(),
                offreEntity.getTitrePoste(),
                io.ekip.implicactioncore.dto.TypeContrat.valueOf(offreEntity.getTypeContrat().name()),
                offreEntity.getDescription(),
                offreEntity.getEmail(),
                offreEntity.getNomEntreprise(),
                offreEntity.getUrlEntreprise(),
                offreEntity.getDescEntreprise(),
                offreEntity.getUrlVideo(),
                offreEntity.getTwitter(),
                offreEntity.getLieuPoste(),
                io.ekip.implicactioncore.dto.StatutOffre.valueOf(offreEntity.getStatut().name().toUpperCase())
        );
    }
}
