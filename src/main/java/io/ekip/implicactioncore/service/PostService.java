package io.ekip.implicactioncore.service;

import io.ekip.implicactioncore.adapter.PostMapper;
import io.ekip.implicactioncore.dto.PostResponse;
import io.ekip.implicactioncore.dto.exception.ImplicRuntimeException;
import io.ekip.implicactioncore.dto.form.PostForm;
import io.ekip.implicactioncore.model.GroupEntity;
import io.ekip.implicactioncore.model.PostEntity;
import io.ekip.implicactioncore.model.UserEntity;
import io.ekip.implicactioncore.repository.GroupRepository;
import io.ekip.implicactioncore.repository.PostRepository;
import io.ekip.implicactioncore.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PostService {
    PostRepository postRepository;
    GroupRepository groupRepository;
    UserRepository userRepository;

    public PostResponse savePost(PostForm postForm, Long groupId, String userEmail) {

        Optional<UserEntity> maybeUser = userRepository.getByEmail(userEmail);
        UserEntity userEntity = maybeUser.get();

        Optional<GroupEntity> maybeGroup = groupRepository.findById(groupId);

        return maybeGroup.map(group -> {
            PostEntity postEntity = PostMapper.entityFromForm(postForm, userEntity, LocalDate.now(), group);
            postRepository.save(postEntity);
            return PostMapper.responseFromEntity(postEntity);
        }).orElseThrow(() -> new ImplicRuntimeException("Aucun groupe n'existe pour cet id: " + groupId));
    }

    public PostResponse getPostById(Long id) {
        Optional<PostEntity> maybePost = postRepository.findById(id);
        return maybePost
                .map(PostMapper::responseFromEntity)
                .orElseThrow(() -> new ImplicRuntimeException("Aucun post n'existe pour cet id: " + id));
    }

}
