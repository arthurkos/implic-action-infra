package io.ekip.implicactioncore.service;

import io.ekip.implicactioncore.adapter.GroupMapper;
import io.ekip.implicactioncore.dto.GroupResponse;
import io.ekip.implicactioncore.dto.UserResponse;
import io.ekip.implicactioncore.dto.exception.ImplicRuntimeException;
import io.ekip.implicactioncore.model.GroupEntity;
import io.ekip.implicactioncore.model.InGroupEntity;
import io.ekip.implicactioncore.model.InGroupKey;
import io.ekip.implicactioncore.repository.GroupRepository;
import io.ekip.implicactioncore.repository.InGroupRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class InGroupService {
    private final InGroupRepository inGroupRepository;
    private GroupRepository groupRepository;

    public GroupResponse subscribeGroup(Long idGroup, UserResponse user) {
        InGroupEntity inGroupEntity = new InGroupEntity(user.getId(), idGroup, false);
        inGroupRepository.save(inGroupEntity);
        Optional<GroupEntity> maybegroupEntity = groupRepository.findById(idGroup);

        return maybegroupEntity
                .map(GroupMapper::ResponseFromEntity)
                .orElseThrow(() -> new ImplicRuntimeException("Aucun groupe n'existe pour cet id: " + idGroup));
    }

    public GroupResponse unsubscribeGroup(Long idGroup, UserResponse user) {
        InGroupKey id = new InGroupKey(user.getId(), idGroup);
        inGroupRepository.deleteById(id);
        Optional<GroupEntity> maybegroupEntity = groupRepository.findById(idGroup);

        return maybegroupEntity
                .map(GroupMapper::ResponseFromEntity)
                .orElseThrow(() -> new ImplicRuntimeException("Aucun groupe n'existe pour cet id: " + idGroup));

    }

    public boolean isSubscribeGroup(Long idGroup, Long user) {
        InGroupKey id = new InGroupKey(user, idGroup);
        boolean isSubscribe = inGroupRepository.existsById(id);
        return isSubscribe;
    }

    public boolean isModo(Long idGroup, UserResponse user) {
        InGroupKey id = new InGroupKey(user.getId(), idGroup);
        return inGroupRepository.findById(id).map(InGroupEntity::getIsModo).orElse(false);
    }

    public void setModo(Long idGroup, Long user) {
        boolean isSubscribe = isSubscribeGroup(idGroup, user);
        if (isSubscribe) {
            Optional<InGroupEntity> inGroupEntity = inGroupRepository.findById(new InGroupKey(user, idGroup));
            inGroupEntity.ifPresent(inGroup -> {
                inGroup.setIsModo(true);
                inGroupRepository.save(inGroup);
            });
        } else {
            InGroupEntity inGroupEntity = new InGroupEntity(user, idGroup, true);
            inGroupRepository.save(inGroupEntity);
        }
    }
}
